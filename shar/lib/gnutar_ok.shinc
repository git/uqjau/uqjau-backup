#!/bin/false -meant2bSourced

# --------------------------------------------------------------------
# Synopsis: Filter for tar STDERR to decide if tar w/nonzero exit 
#           stat was still OK. Return 0 and no STDOUT or STDERR,
#           unless tar errors 'serious'.
# --------------------------------------------------------------------
# Usage: $ourname [--static] < TAR_STDERR_LOG_FILE
#        --static == file-system not expected to change during tar
# --------------------------------------------------------------------
# Rating: tone: tool used: weekly stable: y TBDs: n 
# -------------------------------------------------------------------- 

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2009/10/21 13:08:59 $   (GMT)
# $Revision: 1.6 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/backup/shar/lib/RCS/gnutar_ok.shinc,v $
#      $Log: gnutar_ok.shinc,v $
#      Revision 1.6  2009/10/21 13:08:59  rodmant
#      *** empty log message ***
#
#      Revision 1.5  2009/07/30 14:17:53  rodmant
#      *** empty log message ***
#
#      Revision 1.4  2009/07/30 13:39:36  rodmant
#      *** empty log message ***
#
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) Jul 2009 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# -- Software License --
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---

gnutar_ok() {
    : ==================================================================== 
    : Synopsis: filter to ignore acceptable tar warnings in STDERR from tar.
    : If any unrecognized abnormal STDERR lines exist return 1.
    : ==================================================================== 

    local static
    case ${1:-} in
        --static)
            # file system expected to be static
            # single user mode for example
            static=1
        ;;
    esac

    # handy example log snip
    #   tar: Removing leading `/' from member names
    #   tar: /adm/bin/sys/s/RCS/privatedirs_list.shinc_Vnext,v: Cannot open: Permission denied
    #   tar: Error exit delayed from previous errors
    #   tar: ./home/johndoe/.links/socket: socket ignored
    #   tar: ./home/lucy/Mail/procmail.log: file changed as we read it
    #   tar: Removing leading `/' from hard link targets

    local regex_filter_file=$(mktemp /tmp/$FUNCNAME.regex_filter_file.XXXXXXXX)
    sed -e 's~^        ^~~' >$regex_filter_file <<\____endofhd
        ^tar: .*: Cannot open: Permission denied$
        ^tar: Removing leading `/' from member names$
        ^tar: .*/tmp/.+: Cannot stat: No such file or directory
        ^tar: Error exit delayed from previous errors$
        ^tar: .*: socket ignored$
        ^tar: Removing leading `/' from hard link targets
____endofhd

    # TBD do we need ability to exclude above 'Cannot open: Permission denied'?

    if [[ ${static:-0} == 1 ]]
    then
        : We will expect file system is quiet.
    else
        # Filesystem considered live / changing.

        # ex
        #     tar: ./local/team/mke/log/lj/_jm,make,RunningOrKilled,n7s,1o1vbr2.14102.bqQkJ1qL: File removed before we read it

        sed -e 's~^            ~~' >>$regex_filter_file <<\________eohd
            ^tar: .*: file changed as we read it$
            ^tar: .*: File removed before we read it
            ^tar: .*tracker-store.journal: File shrank by [0-9]+ bytes; padding with zeros
            ^tar: .*sqlite.*.journal: File shrank by [0-9]+ bytes; padding with zeros
________eohd

    fi

    ## main
    if err=$(egrep -v -f "$regex_filter_file")
    then
        # found unexpected STDERR, ie possible real trouble
        {
            echo "$FUNCNAME: found unexpected STDERR:"
            echo "$err"|sed -e 's~^~  ~'
            echo
        } >&2
        rm -f "$regex_filter_file"
        return 1
    else
        rm -f "$regex_filter_file"
        return 0
    fi

    # end: gnutar_ok
}

# : Ex
# 
#     set -e
#     set -x
#     (
#       (
#         (tar $tarargs 2>$tar_err)
#         exit $?
#       ) || gnutar_ok < $tar_err
#     )
#     retval=$?

