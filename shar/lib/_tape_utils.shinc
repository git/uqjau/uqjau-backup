#!/bin/false -meant2bsourced

# -------------------------------------------------------------------{
# Synopsis: shell functions I have used for DDS[23] drives
#   - (overloaded) rsh
#   - _mt
#   - get_tapefile_index
#   - rewind_1_file
#   - (overloaded) tar
#   - untar_1tapefile_to_devnull
#   - tar_filesystem
# --------------------------------------------------------------------
# Env:
#   tape_host                 Leave undefined if tape drive is local.
# 
#   _use_ssh                  If tape drive is remote, set to:
#                             /usr/bin/ssh
# --------------------------------------------------------------------

# in general, 'sleep 5' before any tape movement started

# --------------------------------------------------------------------
# $Source: /usr/local/7Rq/package/cur/backup-2011.05.26/shar/lib/RCS/_tape_utils.shinc._m4,v $
# $Date: 2012/07/22 12:14:07 $ GMT    $Revision: 1.5 $
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2009, 2010, 2011, 2017 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

type -a gnutar_ok &>/dev/null ||
  source $_29lib/gnutar_ok.shinc

export -f gnutar_ok

rsh() {
    # -------------------------------------------------------------------- 
    # Synopsis: overload 'rsh' as ssh for use with tar, when tarring to
    #   remote tape drive
    # -------------------------------------------------------------------- 
    # tar may call 'rsh' to write to remote tape drive.
    
    : $FUNCNAME is wrapper which usually calls ssh instead of rsh.

    if [[ -n "${_use_ssh:-}" ]]; then
        ssh "$@"
    else
        command rsh "$@"
    fi
    return $?
}

_mt() {
    # -------------------------------------------------------------------- 
    # Synopsis: wrapper for mt when using to remote tape drive; uses
    #   ssh to remotely run mt
    # -------------------------------------------------------------------- 
    local cmd=${*:-stat}
    local retval
    (set -x;: In $FUNCNAME.; sleep 10)
      # voodo or not? My theory is tape drives are mechanical
      # so the delay may help prevent tangled tapes (no proof/no harm).

    date >&2
    local tmpf=$(mktemp /tmp/$FUNCNAME.XXXXXXX)

    if [[ -z ${tape_host:-} ]];then

        ( set -x; mt -f ${tape_dev:-/dev/nst0} $cmd ) && retval=0 || retval=$?

            # $ ( bash -c 'exit 111' ) && date || echo $?
            # 111 

    else
        (
            set -x
            rsh "${tape_host}" \
                set '-x;' mt -f ${tape_dev:-/dev/nst0} $cmd';' echo mt returned: '$?' >$tmpf
        )
        retval=$(_grep_integer_field '^mt returned: (\d+)' <$tmpf)
        if ! [[ $retval == 0 ]];then
            echo "$FUNCNAME:ERROR when running [$cmd]" >&2
            sed -e 's~^~  ~' < $tmpf >&2
        else
            cat $tmpf
        fi
    fi

    echo
    rm -f $tmpf
    return $retval

    # end _mt()
}

get_tapefile_index() {
    # -------------------------------------------------------------------- 
    # SYNOPSIS
    #     Echos tape index. Tapeindex is 0 at start of tape, 1 after skipping 
    #     1st tape file.
    # -------------------------------------------------------------------- 
    local retval

    local tmpf=$(mktemp /tmp/$FUNCNAME.XXXXXXX)

    ( set -x; : running $FUNCNAME )

    _mt stat > $tmpf
    if _grep_integer_field 'File number=(-?\d+),' < $tmpf; then
        rm -f $tmpf
        return 0
    else
        {
            echo $FUNCNAME:ERROR:
            sed -e 's~^~  ~' < $tmpf
        } >&2

        return 1
    fi
}

_get_block_size() {
  # -------------------------------------------------------------------- 
  # Synopsis: (tape block size) TBD.. In variable block mode
  # (setblk 0), there appears to be a maximum block size.  For my
  # 1st DDS3 tape it is 16K.
  # -------------------------------------------------------------------- 
  # ( Only for local tapedrive )

  local TMPF=$(mktemp /tmp/$ourname.$FUNCNAME.XXXXXXXX)

  sleep 5
  dd bs=300K if=$_tape_dev count=1 2>&1 >/dev/null |
  tee $TMPF|
  perl -ne '
    if(m{^(\d+) bytes})
    {
      $blocksize = $1;
      $ok++;
    }
    END {
      if ($ok == 1)
      { print "$blocksize\n";}
      else
      { $?=1;
        print STDERR "'$FUNCNAME':$blocksizeERROR/failed\n";
      }
    }
  '

  local _stat=$[ ${PIPESTATUS[0]} + ${PIPESTATUS[1]} + ${PIPESTATUS[2]} ]

  [[ $_stat = 0 ]] ||
  { echo $FUNCNAME:ERROR:dd output:
    sed -e 's~^~  ~' $TMPF
    return $_stat
  }
}

_GET_BLK_SIZE() {
  # -------------------------------------------------------------------- 
  # Synopsis: calls _get_block_size, sets SHELL var BLKSIZE, tries to
  # have no impact on tape position
  # TBD -- does this work, is it used?!
  # -------------------------------------------------------------------- 
  BLKSIZE=$(_get_block_size)
    # global needed for dd

  [[ $BLKSIZE = 0 ]] && { echo $FUNCNAME:ERROR BLKSIZE: $BLKSIZE; return 1; } || :

  get_tape_pos
    # OOPS/TBD: where is this defined
    # defines $filenum

  # goal: return tape to where it was before this function was called:
  if [[ $filenum = 0 ]] ;then
    mt -f /dev/nst0 rewind
  else
    mt -f /dev/nst0 bsfm 1
  fi
}

rewind_1_file() {
  # -------------------------------------------------------------------- 
  # Synopsis: rewind 1 tape file. Calls _mt.
  # -------------------------------------------------------------------- 

  # derived from empirical tests, 
  # too bad '_mt bsfm 2' does not work for all cases

  ( set -x; : running $FUNCNAME)
  local tapeindex=$(get_tapefile_index)
  case $tapeindex in
    [01])
       _mt rewind
    ;;
    *)
       (
       set -x
       : mt man page:
       : bsfm   Backward space past count file marks,  then  forward  space  one
       : file record.  This leaves the tape positioned on the first block
       : of the file that is count-1 files before the current file.
       )
       _mt bsfm 2
    ;;
  esac
  echo
}

_tar() {
    # -------------------------------------------------------------------- 
    # Synopsis: GNU tar wrapper to support remote tape drive
    # -------------------------------------------------------------------- 
    (
        set -x
        sleep 5
        time tar \
            --one-file-system \
            --exclude '*,RunningOrKilled,*' \
            ${_use_ssh+--rsh-command=$_use_ssh} "$@"
                # _use_ssh if defined is path to ssh, typically /usr/bin/ssh
    )
}

untar_1tapefile_to_devnull() {

    # -------------------------------------------------------------------- 
    # Synopsis: untar to no where, just count chars
    # -------------------------------------------------------------------- 
    ( 
        set -x
        : read tape file just written
        : -O == send tar extract output to STDOUT
        if [[ -z ${tape_host:-} ]] ;then
            (
                set -x
                sleep 5
                date
                time tar \
                    --exclude '*,RunningOrKilled,*' \
                    -zOxf "${tape_dev}" |
                dd >/dev/null

                # info tar:
                #   `-O'
                #      `--to-stdout'
            )
        else
            ssh "$tape_host" tape_dev=$tape_dev bash <<\____________eohd
                set -x
                sleep 5
                date
                time tar \
                    --exclude '*,RunningOrKilled,*' \
                    -zOxf "${tape_dev}" |
                dd >/dev/null
____________eohd

        fi
    )

    # tar in this case leaves tape position at same location
    # that 'mt -f ${tape_host:+${tape_host}:}${tape_dev} fsf 1' would have

    # dd's only function above is to count bytes

    echo
}

#  90 meter == 295.27559010 feet  #  * 90 *100 * 1/2.54/12
# 112 meter == 367.45406768 feet 
# 120 meter == 393.70078680 feet

# DDS1  ~577.066 K bytes/in    590916 bpi;echo '1.3 * 2^30 /(60 meter  * 100 * 1/2.54)'|bc
# max dump density allowed is ~327600 (dump command aborts for higher densities)

# from www.cis.ohio-state.edu/hypertext/faq/usenet/hp/hpux-faq/faq-doc-162.html
# let density == 6250
# blocking factor default is 32
# assumed IRG (inter record gap) == .7 in
# block length = (32*2^10 bytes/block)/(6250 bytes/inch) +.7 = 5.54 # echo '32*2^10/6250 +.3' | bc
# effective tape length = 2.0*2^30 /(32*2^10)*5.54/12 = 30256 feet # 2GB  90 meter DDS1 tape
# effective tape length = 4.0*2^30 /(32*2^10)*5.54/12 = 60512 feet # 4GB 120 meter DDS2 tape

tar_filesystem() {
    # -------------------------------------------------------------------- 
    # Synopsis: tar a directory, staying in it's filesystem.  Modify exit
    # code, by running tar STDERR output through 'gnutar_ok'.  This function
    # 'cd's to the directory before starting tar.
    # -------------------------------------------------------------------- 
    local partition=$1
    local retval
    local tar_retval
    local tar_err=$(mktemp /tmp/$FUNCNAME.tar_err.XXXXXXXX)

    tarargs="--one-file-system --atime-preserve -zcf ${tape_host:+${tape_host}:}${tape_dev} ."  
    date
    cd $partition

    set -x

    #run tar!
    : CWD: [$PWD] $FUNCNAME running:
    (
        (
            (
                sleep 5
                time tar \
                    ${_use_ssh+--rsh-command=$_use_ssh} \
                    ${tar_filesystem_cfgfile_args:-} \
                    $tarargs 2>$tar_err
            )

            tar_stat=$?
                # we will see above assignment in log

            if [[ ${testrun:-} == 1 ]] ;then
                : To force gnutar_ok test.
                builtin exit 1
            else
                : '$testrun *not* set, so is a real run.'
            fi

            builtin exit $tar_stat
        ) || ( bash -c "gnutar_ok < $tar_err" )
             # CAREFUL: above '||' disables 'set -e' in preceding block
    )
    retval=$?

    set +x

    date
    test -s "$tar_err" && (cat $tar_err;echo) || rm -f "$tar_err"
    echo
    return $retval

    # end tar_filesystem
}

