head	1.1;
access;
symbols;
locks
	rodmant:1.1; strict;
comment	@# @;


1.1
date	2011.12.10.20.08.52;	author rodmant;	state Exp;
branches;
next	;


desc
@@


1.1
log
@Initial revision
@
text
@# -------------------------------------------------------------------{
# Synopsis: shell functions I have used for DDS[23] drives
#   - (overloaded) rsh
#   - _mt
#   - get_tapefile_index
#   - rewind_1_file
#   - (overloaded) tar
#   - untar_1tapefile_to_devnull
#   - tar_filesystem
# --------------------------------------------------------------------
# Usage:
# --------------------------------------------------------------------
# Options:
# Examples:
# Files:
#   external tools invoked, config files
# Environment:
#   _use_ssh=/usr/bin/ssh   
#     # rsh is overloaded as ssh (see below 'rsh' function)
#     # also _tar calls tar with "--rsh-command=$_use_ssh"
# Description:
# Logs:
# Error codes:
# Globals:
# Inputs:
# Outputs:
# --------------------------------------------------------------------


# --------------------------------------------------------------------
# $Source: /usr/local/7Rq/package/cur/backup/shar/lib/RCS/_tape_utils.shinc._m4,v $
# $Date: 2011/05/26 02:51:19 $ GMT    $Revision: 1.4 $
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2009, 2010, 2011 Tom Rodman <Rodman.T.S@@gmail.com>
#
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

type -a gnutar_ok &>/dev/null ||
  source $_29lib/gnutar_ok.shinc

export -f gnutar_ok

# tar may call 'rsh' to write to remote tape drive
rsh()
{
  if [[ -n "${_use_ssh:-}" ]]; then
    ssh "$@@"
  else
    command rsh "$@@"
  fi
}

_mt()
{
  local cmd=${*:-stat}
  local retval
  sleep 5
    # voodo or not? My theory is tape drives are mechanical
    # so the delay may help prevent tangled tapes (no proof/no harm).

  local tmpf=$(mktemp /tmp/$FUNCNAME.XXXXXXX)

  rsh "${tape_host}" \
    set '-x;' mt -f ${tape_dev:-/dev/nst0} $cmd';' echo mt returned: '$?' >$tmpf
  retval=$(_grep_integer_field '^mt returned: (\d+)' <$tmpf)

  cat $tmpf
  echo
  rm -f $tmpf
  return $retval
}

get_tapefile_index()
{
  # tapeindex is 0 at start of tape, 1 after skipping 1st tape
  local retval

  local tmpf=$(mktemp /tmp/$FUNCNAME.XXXXXXX)
  _mt stat >$tmpf
  _grep_integer_field 'File number=(-?\d+),' < $tmpf &&
    rm -f $tmpf
}

rewind_1_file()
{
  # derived from empirical tests, 
  # too bad '_mt bsfm 2' does not work for all cases

  local tapeindex=$(get_tapefile_index)
  case $tapeindex in
    [01])
       _mt rewind
    ;;
    *)
       _mt bsfm 2
    ;;
  esac
}

_tar()
{
  (tar ${_use_ssh+--rsh-command=$_use_ssh} "$@@")
    # _use_ssh if defined is path to ssh, typically /usr/bin/ssh
}

untar_1tapefile_to_devnull()
{
  (set -x; _tar -zOxf ${tape_host}:${tape_dev} |wc -c)
  # info tar:
  #   `-O'
  #      `--to-stdout'

  # tar in this case leaves tape position at same location
  # that 'mt -f ${tape_host}:${tape_dev} fsf 1' would have

  # wc's only function above is to count bytes

  echo
}

#  90 meter == 295.27559010 feet  #  * 90 *100 * 1/2.54/12
# 112 meter == 367.45406768 feet 
# 120 meter == 393.70078680 feet

# DDS1  ~577.066 K bytes/in    590916 bpi;echo '1.3 * 2^30 /(60 meter  * 100 * 1/2.54)'|bc
# max dump density allowed is ~327600 (dump command aborts for higher densities)

# from www.cis.ohio-state.edu/hypertext/faq/usenet/hp/hpux-faq/faq-doc-162.html
# let density == 6250
# blocking factor default is 32
# assumed IRG (inter record gap) == .7 in
# block length = (32*2^10 bytes/block)/(6250 bytes/inch) +.7 = 5.54 # echo '32*2^10/6250 +.3' | bc
# effective tape length = 2.0*2^30 /(32*2^10)*5.54/12 = 30256 feet # 2GB  90 meter DDS1 tape
# effective tape length = 4.0*2^30 /(32*2^10)*5.54/12 = 60512 feet # 4GB 120 meter DDS2 tape

tar_filesystem() {
  local partition=$1
  local retval
  local tar_retval
  local tar_err=$(mktemp /tmp/$FUNCNAME.tar_err.XXXXXXXX)

  tarargs="--one-file-system --atime-preserve -zcf ${tape_host}:${tape_dev} ."  
  date
  cd $partition

  set -x

  #run tar!
  : CWD: [$PWD] $FUNCNAME running:
  (
    (
      (tar ${_use_ssh+--rsh-command=$_use_ssh} $tarargs 2>$tar_err)

      tar_stat=$?
        # we will see above assignment in log

      test "${testrun:-}" = 1 && builtin exit 1
        # to force gnutar_ok test

      builtin exit $tar_stat
    ) || ( bash -c "gnutar_ok < $tar_err" )
  )
  retval=$?

  set +x

  date
  test -s "$tar_err" && (cat $tar_err;echo) || rm -f "$tar_err"
  return $retval

  # end tar_filesystem
}

@
