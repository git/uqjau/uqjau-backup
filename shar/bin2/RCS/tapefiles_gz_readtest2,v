head	1.10;
access;
symbols;
locks
	rodmant:1.10; strict;
comment	@# @;


1.10
date	2015.11.07.13.55.06;	author rodmant;	state Exp;
branches;
next	1.9;

1.9
date	2011.12.03.17.28.06;	author rodmant;	state Exp;
branches;
next	1.8;

1.8
date	2011.12.03.17.25.30;	author rodmant;	state Exp;
branches;
next	1.7;

1.7
date	2011.12.02.20.02.28;	author rodmant;	state Exp;
branches;
next	1.6;

1.6
date	2011.11.28.03.52.47;	author rodmant;	state Exp;
branches;
next	1.5;

1.5
date	2011.11.28.02.44.26;	author rodmant;	state Exp;
branches;
next	1.4;

1.4
date	2011.11.28.02.27.26;	author rodmant;	state Exp;
branches;
next	1.3;

1.3
date	2011.11.27.15.23.54;	author rodmant;	state Exp;
branches;
next	1.2;

1.2
date	2011.11.27.15.10.19;	author rodmant;	state Exp;
branches;
next	1.1;

1.1
date	2011.11.27.00.26.15;	author rodmant;	state Exp;
branches;
next	;


desc
@@


1.10
log
@*** empty log message ***
@
text
@#!/usr/bin/env bash
set -eu

# --------------------------------------------------------------------
# Synopsis: count tape files, then in parallel: dd each one, counting bytes
#   and gzip -d to md5sum.
#   May be used in recovery, so keep it stand alone.
# --------------------------------------------------------------------
# Assumption: each tape file is gzipped
# --------------------------------------------------------------------
# Usage: $ourname [TAPEFILE_COUNT]
#   uses env var _tape_dev, set this else defaults to /dev/nst0
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) Sep 2011 Tom Rodman <Rodman.T.S@@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2011/12/03 17:28:06 $   (GMT)
# $Revision: 1.9 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/backup-2011.05.26/shar/bin2/RCS/tapefiles_gz_readtest2,v $
#      $Log: tapefiles_gz_readtest2,v $
#      Revision 1.9  2011/12/03 17:28:06  rodmant
#      *** empty log message ***
#
#      Revision 1.8  2011/12/03 17:25:30  rodmant
#      *** empty log message ***
#
#      Revision 1.7  2011/12/02 20:02:28  rodmant
#      *** empty log message ***
#
#      Revision 1.6  2011/11/28 03:52:47  rodmant
#      *** empty log message ***
#
# --------------------------------------------------------------------

ourname=${0##*/}
jobid=$$

get_tape_pos()
{
  # tapeindex is 0 at start of tape, 1 after skipping 1st tape

  unset filenum blknum
  local tmpf=$(mktemp /tmp/$FUNCNAME.XXXXXXX)

  mt -f $_tape_dev stat >$tmpf

  set -- $(
    perl -ne '
      # handy data example from mt:
      # File number=0, block number=1, partition=0.

      if(m{^File number=(-?\d+),\s+block number=(-?\d+)})
      { ($filenum,$blknum) = ($1,$2); }

      END
      { if (defined($filenum) and defined($blknum) )
        { print "$filenum $blknum\n";}
        else
        { $?=1; }
      }
    ' < $tmpf || kill 0
  )
  rm -f $tmpf

  # set two global vars
  filenum=$1 blknum=$2

}

_get_block_size()
{ # In variable block mode (setblk 0), there appears to be a maximum block size.
  # For my 1st DDS3 tape it is 16K.

  local TMPF=$(mktemp /tmp/$ourname.$FUNCNAME.XXXXXXXX)

  dd bs=300K if=$_tape_dev count=1 2>&1 >/dev/null |
  tee $TMPF|
  perl -ne '
    if(m{^(\d+) bytes})
    {
      $blocksize = $1;
      $ok++;
    }
    END {
      if ($ok == 1)
      { print "$blocksize\n";}
      else
      { $?=1;
        print STDERR "'$FUNCNAME':$blocksizeERROR/failed\n";
      }
    }
  '

  local _stat=$[ ${PIPESTATUS[0]} + ${PIPESTATUS[1]} + ${PIPESTATUS[2]} ]

  [[ $_stat = 0 ]] ||
  { echo $FUNCNAME:ERROR:dd output:
    sed -e 's~^~  ~' $TMPF
    return $_stat
  }
}

_GET_BLK_SIZE()
{
  BLKSIZE=$(_get_block_size)
    # global needed for dd

  [[ $BLKSIZE = 0 ]] && { echo $FUNCNAME:ERROR BLKSIZE: $BLKSIZE; return 1; } || :

  get_tape_pos
    # defines $filenum

  # goal: return tape to where it was before this function was called:
  if [[ $filenum = 0 ]] ;then
    mt -f /dev/nst0 rewind
  else
    mt -f /dev/nst0 bsfm 1
  fi
}

_reset_tape_drive()
{
  mt -f /dev/nst0 defcompression 0
  mt -f /dev/nst0 defblksize 0
    # sets variable block size ?? 
    # explain difference w/ 'mt -f /dev/nst0 setblk 0'

  mt -f /dev/nst0 stoptions buffer-writes async-writes read-ahead
}

get_tape_file_count ()
{
  # ------------------------------------------------------------------------
  # count tape files
  #   sets global var: file_count
  # ------------------------------------------------------------------------

  # advance to end of data
  mt -f /dev/nst0 eod
  get_tape_pos
  filenum_4endof_tape=$filenum
    # $filenum_4endof_tape refers to start of nonexistent file, so turns out that
    # $filenum_4endof_tape == total number of valid files
  file_count=$filenum_4endof_tape

  if test "$[$file_count + 0]" -lt 1
  then
    echo $ourname: no tape files found
    mt -f $_tape_dev stat
    exit 1
  else
    echo "$ourname: found [$file_count] tape files"
    echo
  fi

  echo -n "rewinding .."
  mt -f /dev/nst0 rewind
  echo done
}

# **************************************************
# Main procedure.
# **************************************************
#main
{

: ${_tape_dev:=/dev/nst0}

echo -n "rewinding .."
mt -f /dev/nst0 rewind
echo done

#file_count=2
echo counting tape files..
get_tape_file_count
  # sets global var file_count
echo

# allow user to lower the number of tape files (from start of tape) read
# by supplying 1 CLI arg for file_count
[[ -n $1 ]] &&
{ user_file_count=$1
  (( $user_file_count < $file_count )) && file_count=$user_file_count || :
} || :

fifo=/tmp/$ourname.$jobid
mkfifo $fifo

## read each tape file
filesreadok=0
TMPFgzip=$(mktemp /tmp/$ourname.gzip.XXXXXXXX)
  TMPFdd=$(mktemp /tmp/$ourname.dd.XXXXXXXX)

trap 'rm -f $fifo $TMPFgzip $TMPFdd ' EXIT

MAXBLOCKSIZE=16K
  # Need to experiment w/your tape drive to get this value ... TBD explain.
  # ( _GET_BLK_SIZE may be helpful )

## read the tape:
echo read tape files..
echo
for (( i=1; $i <= $file_count ;i += 1))
do 
  
  #_GET_BLK_SIZE

  echo "### $ourname: about to read tape file [$[$i -1]+/$[$file_count -1]+] [$(date)]"
  echo

  echo status prior to test:
  mt -f $_tape_dev status|sed -e 's~^~  ~'
  echo

  (
    echo reading tape w/gzip -d to md5sum
    echo also counting uncompressed bytes and xfr rate w/dd read
    echo

    time gzip -d < $_tape_dev 2>$TMPFgzip | tee $fifo |md5sum &

    dd bs=$MAXBLOCKSIZE  < $fifo >/dev/null 2>$TMPFdd &
      # dd gives us total size and xfr rate

    wait
    stat=$?
    echo
    exit $stat
  ) || { stat=$?; set -x; exit $stat; }

  if [[ -s $$TMPFgzip ]];then
    echo gzip stderr:
    sed -e 's~^~  ~' $TMPFgzip
    echo
  fi

  echo dd read of uncompressed data:
  sed -e 's~^~  ~' $TMPFdd

  # TBD
  [[ $i = 3 ]] && exit

  let filesreadok++ || :
  echo
done

if test "$file_count" = "$filesreadok"
then
  mt -f $_tape_dev rewind
  echo "$ourname: done OK [$filesreadok/$file_count] tape files [$(date)]"
  exit 0
else
  echo "files read ok: [$filesreadok/$file_count]"
  echo "$ourname: done w/ERROR(s) [$(date)]"
  exit 1
fi

# ==================================================
# main done.
# ==================================================
: dropping through to main end #set -x makes this echo

} #main end
@


1.9
log
@*** empty log message ***
@
text
@d35 2
a36 2
#     $Date: 2011/12/03 17:25:30 $   (GMT)
# $Revision: 1.8 $
d41 3
d203 1
a203 1
  [[ $user_file_count < $file_count ]] && file_count=$user_file_count || :
@


1.8
log
@*** empty log message ***
@
text
@d35 2
a36 2
#     $Date: 2011/12/02 20:02:28 $   (GMT)
# $Revision: 1.7 $
d41 3
a49 13
#      Revision 1.5  2011/11/28 02:44:26  rodmant
#      *** empty log message ***
#
#      Revision 1.4  2011/11/28 02:27:26  rodmant
#      *** empty log message ***
#
#      Revision 1.3  2011/11/27 15:23:54  rodmant
#      *** empty log message ***
#
#      Revision 1.1  2011/11/27 00:26:15  rodmant
#      Initial revision
#
#
d201 1
a201 1
}
@


1.7
log
@*** empty log message ***
@
text
@d11 1
a11 1
# Usage: $ourname
d16 1
a16 1
# Copyright (c) Sep 2009, 2010, 2011 Tom Rodman <Rodman.T.S@@gmail.com>
d35 2
a36 2
#     $Date: 2011/11/28 03:52:47 $   (GMT)
# $Revision: 1.6 $
d39 5
a43 2
#   $Source: /usr/local/7Rq/package/cur/backup-2011.05.26/shar/bin2/RCS/tapefiles_readtest2,v $
#      $Log: tapefiles_readtest2,v $
d162 1
d203 1
d206 7
@


1.6
log
@*** empty log message ***
@
text
@d35 2
a36 2
#     $Date: 2011/11/28 02:44:26 $   (GMT)
# $Revision: 1.5 $
d41 3
d223 1
a223 1
  echo "### $ourname: about to read tape file [$i/$file_count] [$(date)]"
@


1.5
log
@*** empty log message ***
@
text
@d35 2
a36 2
#     $Date: 2011/11/28 02:27:26 $   (GMT)
# $Revision: 1.4 $
d41 3
d194 1
d196 1
d213 2
d219 4
d226 1
a226 1
  echo "$ourname: reading tape file [$i/$file_count] [$(date)]"
d252 2
a253 1
  # [[ $i = 3 ]] && exit
@


1.4
log
@*** empty log message ***
@
text
@d35 2
a36 2
#     $Date: 2011/11/27 15:23:54 $   (GMT)
# $Revision: 1.3 $
d41 3
a138 70
rewind_1_file()
{
  # derived from empirical tests,
  # too bad '_mt bsfm 2' does not work for all cases

  local tapeindex=$(get_tapefile_index)
  case $tapeindex in
    [01])
       mt -f $_tape_dev rewind
    ;;
    *)
       mt -f $_tape_dev bsfm 2
    ;;
  esac
}

_get_block_size()
{
  
  # In variable block mode (setblk 0), there appears to be a maximum block size.
  # For my 1st DDS3 tape it is 16K.

  local TMPF=$(mktemp /tmp/$ourname.$FUNCNAME.XXXXXXXX)

  dd bs=300K if=$_tape_dev count=1 2>&1 >/dev/null |
  tee $TMPF|
  perl -ne '
    if(m{^(\d+) bytes})
    {
      $blocksize = $1;
      $ok++;
    }
    END {
      if ($ok == 1)
      { print "$blocksize\n";}
      else
      { $?=1;
        print STDERR "'$FUNCNAME':$blocksizeERROR/failed\n";
      }
    }
  '

  local _stat=$[ ${PIPESTATUS[0]} + ${PIPESTATUS[1]} + ${PIPESTATUS[2]} ]

  [[ $_stat = 0 ]] ||
  { echo $FUNCNAME:ERROR:dd output:
    sed -e 's~^~  ~' $TMPF
    return $_stat
  }

}

_GET_BLK_SIZE()
{
  BLKSIZE=$(_get_block_size)
    # global needed for dd

  [[ $BLKSIZE = 0 ]] && { echo $FUNCNAME:ERROR BLKSIZE: $BLKSIZE; return 1; } || :

  get_tape_pos
    # defines $filenum

  # goal: return tape to where it was before this function was called:
  if [[ $filenum = 0 ]] ;then
    mt -f /dev/nst0 rewind
  else
    mt -f /dev/nst0 bsfm 1
  fi
}

d192 1
d203 5
d223 1
a223 1
    dd bs=16K  < $fifo >/dev/null 2>$TMPFdd &
a224 1
      # Need to experiment to get this value of 16K ... TBD explain.
@


1.3
log
@*** empty log message ***
@
text
@d5 2
a6 1
# Synopsis: count tape files, then dd each one, counting bytes
d9 2
d35 2
a36 2
#     $Date: 2011/11/27 00:26:15 $   (GMT)
# $Revision: 1.1 $
d41 3
d85 51
a187 18
_rewind()
{
  local ret_val
  (set -x;mt -f $_tape_dev rewind) ; ret_val=$?
  echo
  return $ret_val
}

_reset_tape_drive()
{
  mt -f /dev/nst0 defcompression 0
  mt -f /dev/nst0 defblksize 0
    # sets variable block size ?? 
    # explain difference w/ 'mt -f /dev/nst0 setblk 0'

  mt -f /dev/nst0 stoptions buffer-writes async-writes read-ahead
}

d206 10
a256 1
set -x
d265 1
a265 1
TMPFdd=$(mktemp /tmp/$ourname.dd.XXXXXXXX)
d268 1
d272 1
a272 1
  _GET_BLK_SIZE
d278 2
a279 2
    echo reading tape w/gzip -d, w/output to md5sum
    echo also counting uncompressed bytes and xfr rate w/dd read, using bs=$BLKSIZE
d281 1
d283 2
a284 1
    dd bs=$BLKSIZE  < $fifo >/dev/null 2>$TMPFdd &
d286 2
d303 2
a304 1
  [[ $i = 2 ]] && exit
d312 1
a312 1
  echo "$ourname: done ok [$filesreadok/$file_count] tape files [$(date)]"
@


1.2
log
@*** empty log message ***
@
text
@a2 1
jobid=$$
d45 1
a45 56
ourdir=${0%/$ourname}
  # may be useful dir for "rc" config files
Ourname=${0}

_grep_integer_field()
{
  : $FUNCNAME a filter, returns integer, input usually a line or two
  local regex=$1
  # ex regex
  #   '^mt returned: (\d+)'
  local ourstdin=$(cat)
  local perlstat

  (
  echo "$ourstdin"|
  regex=$regex perl -lne '
    BEGIN { $regex = $ENV{regex}; }

    $num = $1 if(m{$regex});

    END
    {
      if ($num =~ m{^-?\d+$})
        { print $num ; }
      else
        {
          exit 1;
        }
    }
  '
  )
  perlstat=$?

  if ! test $perlstat = 0
  then
    {
    echo "${ourname+$ourname:}$FUNCNAME: [$regex]"
    echo "  regex above did not match any STDIN:"
    echo "$ourstdin"|cat -A
    echo
    } >&2
    return $perlstat
  fi

}

Oget_tapefile_index()
{
  # tapeindex is 0 at start of tape, 1 after skipping 1st tape
  local retval

  local tmpf=$(mktemp /tmp/$FUNCNAME.XXXXXXX)
  mt -f $_tape_dev stat >$tmpf
  _grep_integer_field 'File number=(-?\d+),' < $tmpf &&
    rm -f $tmpf
}
a78 1

d98 3
d139 1
a139 18
# **************************************************
# Main procedure.
# **************************************************
#main
{

: ${_tape_dev:=/dev/nst0}

echo -n "rewinding .."
mt -f /dev/nst0 rewind
echo done

# ------------------------------------------------------------------------
# count tape files
# ------------------------------------------------------------------------

# advance to end of data
skip()
d141 4
a144 16
mt -f /dev/nst0 eod
get_tape_pos
filenum_4endof_tape=$filenum
  # $filenum_4endof_tape refers to start of nonexistent file, so turns out that
  # $filenum_4endof_tape == total number of valid files
file_count=$filenum_4endof_tape

if test "$[$file_count + 0]" -lt 1
then
  echo $ourname: no tape files found
  mt -f $_tape_dev stat
  exit 1
else
  echo "$ourname: found [$file_count] tape files"
  echo
fi
d146 1
a146 3
echo -n "rewinding .."
mt -f /dev/nst0 rewind
echo done
a148 3
set -x
file_count=4

d154 1
a154 2
  mt -f /dev/nst0 setblk $BLKSIZE
    # not sure if needed
d165 7
d173 21
d196 5
d202 9
d230 1
a230 1
    echo also counting uncompressed bytes and xfr rate w/dd
d237 1
d241 5
a245 4
  echo
  echo gzip stderr:
  sed -e 's~^~  ~' $TMPFgzip
  echo
a265 91

exit 0

######################################
blksize=$(_get_block_size)
mt -f /dev/nst0 setblk $blksize

get_tapefile_index
exit
mt -f /dev/nst0 status
exit
mt -f /dev/nst0 status
get_tapefile_index
mt -f /dev/nst0 status
mt -f /dev/nst0 fsf 1
mt -f /dev/nst0 status

exit

# ------------------------------------------------------------------------
# count tape files
# ------------------------------------------------------------------------

# advance to end of data
(set -x;mt -f $_tape_dev eod) || exit $?

file_count=$(
  mt -f $_tape_dev stat |perl -lne 'print $1 if (m{^File number=(\d+),});'
  exit ${PIPESTATUS[0]}
) || exit $?
echo

#handy mt stat example:
#  > 11:56:47 Fri Jul 10    /usr/local/7Rq/scommands/cur
#  > 1210 0 olive root # mt -f /dev/nst0 stat
#  SCSI 2 tape drive:
#  File number=3, block number=0, partition=0.
#  Tape block size 512 bytes. Density code 0x24 (DDS-2).
#  Soft error count since last status=0
#  General status bits on (89010000):
#   EOF EOD ONLINE IM_REP_EN

if test "$[$file_count + 0]" -lt 1
then
  echo $ourname: no tape files found
  mt -f $_tape_dev stat
  exit 1
else
  echo "$ourname: found [$file_count] tape files"
  echo
fi

_rewind
#mt -f /dev/st0 stat
#echo "$ourname: stat above after tape rewound [$(date)]"
#echo

fifo=/tmp/$ourname.$jobid
mkfifo $fifo
trap 'rm -f $fifo' EXIT

## read each tape file
filesreadok=0
for (( i=1; $i <= $file_count ;i += 1))
do 
  echo "$ourname: reading tape file [$i/$file_count] [$(date)]"
  (
    set -x
    time gzip -d < $_tape_dev | tee $fifo |md5sum &
    dd bs=200k < $fifo &
      # dd gives us total size and xfr rate
    wait
    stat=$?
    exit $stat
  ) || break
  let filesreadok++ || :
  echo
done

if test "$file_count" = "$filesreadok"
then
  _rewind
  echo "$ourname: done ok [$filesreadok/$file_count] tape files [$(date)]"
  exit 0
else
  _rewind
  echo "files read ok: [$filesreadok/$file_count]"
  echo "$ourname: done w/ERROR(s) [$(date)]"
  exit 1
fi

a271 5

# Processing should never reach this line.
die "EOF scripting error, script should end w/an exit statement"

### End of File ###
@


1.1
log
@Initial revision
@
text
@d33 2
a34 2
#     $Date: 2010/11/26 13:45:22 $   (GMT)
# $Revision: 1.9 $
d37 5
a41 2
#   $Source: /usr/local/7Rq/package/cur/backup/shar/bin/RCS/tapefiles_readtest._m4,v $
#      $Log: tapefiles_readtest._m4,v $
d155 2
a156 1
set -x
d158 1
d175 7
a181 1
  return $[ ${PIPESTATUS[0]} + ${PIPESTATUS[1]} ]
@
