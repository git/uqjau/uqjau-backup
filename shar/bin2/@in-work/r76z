#!/usr/bin/env bash
set -eu

# --------------------------------------------------------------------
# Synopsis: count tape files, then dd each one, counting bytes
#   May be used in recovery, so keep it stand alone.
# --------------------------------------------------------------------
# Usage: $ourname
#   uses env var _tape_dev, set this else defaults to /dev/nst0
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) Sep 2009, 2010, 2011 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# -- Software License --
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2010/11/26 13:45:22 $   (GMT)
# $Revision: 1.9 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/backup/shar/bin/RCS/tapefiles_readtest._m4,v $
#      $Log: tapefiles_readtest._m4,v $
#
# --------------------------------------------------------------------

ourname=${0##*/}
ourdir=${0%/$ourname}
  # may be useful dir for "rc" config files
Ourname=${0}

_grep_integer_field()
{
  : $FUNCNAME a filter, returns integer, input usually a line or two
  local regex=$1
  # ex regex
  #   '^mt returned: (\d+)'
  local ourstdin=$(cat)
  local perlstat

  (
  echo "$ourstdin"|
  regex=$regex perl -lne '
    BEGIN { $regex = $ENV{regex}; }

    $num = $1 if(m{$regex});

    END
    {
      if ($num =~ m{^-?\d+$})
        { print $num ; }
      else
        {
          exit 1;
        }
    }
  '
  )
  perlstat=$?

  if ! test $perlstat = 0
  then
    {
    echo "${ourname+$ourname:}$FUNCNAME: [$regex]"
    echo "  regex above did not match any STDIN:"
    echo "$ourstdin"|cat -A
    echo
    } >&2
    return $perlstat
  fi

}

get_tapefile_index()
{
  # tapeindex is 0 at start of tape, 1 after skipping 1st tape
  local retval

  local tmpf=$(mktemp /tmp/$FUNCNAME.XXXXXXX)

  mt -f $_tape_dev stat >$tmpf

  set - $(
    perl -ne '
      if(m{^File number=(-?\d+),\s+block number=(-?\d+)})
      { ($file_no,$blkno) = ($1,$2); }
      END 
      { if ($file_no and $blkno )
        { print "$file_no $blkno\n";}
        else
        { $?=1; }
      }
    ' < $tmpf 
  )
  rm -f $tmpf

  echo ghjgjh $1 $2

  # handy data example from mt:
  # File number=0, block number=1, partition=0.
}

rewind_1_file()
{
  # derived from empirical tests,
  # too bad '_mt bsfm 2' does not work for all cases

  local tapeindex=$(get_tapefile_index)
  case $tapeindex in
    [01])
       mt -f $_tape_dev rewind
    ;;
    *)
       mt -f $_tape_dev bsfm 2
    ;;
  esac
}

_get_block_size()
{

  dd bs=300K if=$_tape_dev count=1 2>&1 >/dev/null |
  perl -ne '
    if(m{^(\d+) bytes})
    {
      $blocksize = $1;
      $ok++;

    }
    END {
      if ($ok == 1)
      { print "$blocksize\n";}
      else
      { $?=1;
        print STDERR "'$FUNCNAME':$blocksizeERROR/failed\n";
      }
    }
  '
}
_rewind()
{
  local ret_val
  (set -x;mt -f $_tape_dev rewind) ; ret_val=$?
  echo
  return $ret_val
}

# **************************************************
# Main procedure.
# **************************************************
#main
{

: ${_tape_dev:=/dev/nst0}

# ------------------------------------------------------------------------
# count tape files
# ------------------------------------------------------------------------

# advance to end of data
(set -x;mt -f $_tape_dev eod) || exit $?

file_count=$(
  mt -f $_tape_dev stat |perl -lne 'print $1 if (m{^File number=(\d+),});'
  exit ${PIPESTATUS[0]}
) || exit $?
echo

#handy mt stat example:
#  > 11:56:47 Fri Jul 10    /usr/local/7Rq/scommands/cur
#  > 1210 0 olive root # mt -f /dev/nst0 stat
#  SCSI 2 tape drive:
#  File number=3, block number=0, partition=0.
#  Tape block size 512 bytes. Density code 0x24 (DDS-2).
#  Soft error count since last status=0
#  General status bits on (89010000):
#   EOF EOD ONLINE IM_REP_EN

if test "$[$file_count + 0]" -lt 1
then
  echo $ourname: no tape files found
  mt -f $_tape_dev stat
  exit 1
else
  echo "$ourname: found [$file_count] tape files"
  echo
fi

_rewind
#mt -f /dev/st0 stat
#echo "$ourname: stat above after tape rewound [$(date)]"
#echo

fifo=/tmp/$ourname.$jobid
mkfifo $fifo
trap 'rm -f $fifo' EXIT

## read each tape file
filesreadok=0
for (( i=1; $i <= $file_count ;i += 1))
do 
  echo "$ourname: reading tape file [$i/$file_count] [$(date)]"
  (
    set -x
    time gzip -d < $_tape_dev | tee $fifo |md5sum &
    dd bs=200k < $fifo &
      # dd gives us total size and xfr rate
    wait
    stat=$?
    exit $stat
  ) || break
  let filesreadok++ || :
  echo
done

if test "$file_count" = "$filesreadok"
then
  _rewind
  echo "$ourname: done ok [$filesreadok/$file_count] tape files [$(date)]"
  exit 0
else
  _rewind
  echo "files read ok: [$filesreadok/$file_count]"
  echo "$ourname: done w/ERROR(s) [$(date)]"
  exit 1
fi

# ==================================================
# main done.
# ==================================================
: dropping through to main end #set -x makes this echo

} #main end

# Processing should never reach this line.
die "EOF scripting error, script should end w/an exit statement"

### End of File ###
