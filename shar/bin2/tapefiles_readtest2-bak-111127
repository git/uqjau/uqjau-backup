#!/usr/bin/env bash
set -eu

# --------------------------------------------------------------------
# Synopsis: count tape files, then dd each one, counting bytes
#   May be used in recovery, so keep it stand alone.
# --------------------------------------------------------------------
# Usage: $ourname
#   uses env var _tape_dev, set this else defaults to /dev/nst0
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) Sep 2009, 2010, 2011 Tom Rodman <Rodman.T.S@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2011/11/27 15:23:54 $   (GMT)
# $Revision: 1.3 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/backup-2011.05.26/shar/bin2/RCS/tapefiles_readtest2,v $
#      $Log: tapefiles_readtest2,v $
#      Revision 1.3  2011/11/27 15:23:54  rodmant
#      *** empty log message ***
#
#      Revision 1.1  2011/11/27 00:26:15  rodmant
#      Initial revision
#
#
# --------------------------------------------------------------------

ourname=${0##*/}
jobid=$$

get_tape_pos()
{
  # tapeindex is 0 at start of tape, 1 after skipping 1st tape

  unset filenum blknum
  local tmpf=$(mktemp /tmp/$FUNCNAME.XXXXXXX)

  mt -f $_tape_dev stat >$tmpf

  set -- $(
    perl -ne '
      # handy data example from mt:
      # File number=0, block number=1, partition=0.

      if(m{^File number=(-?\d+),\s+block number=(-?\d+)})
      { ($filenum,$blknum) = ($1,$2); }

      END
      { if (defined($filenum) and defined($blknum) )
        { print "$filenum $blknum\n";}
        else
        { $?=1; }
      }
    ' < $tmpf || kill 0
  )
  rm -f $tmpf

  # set two global vars
  filenum=$1 blknum=$2

}

rewind_1_file()
{
  # derived from empirical tests,
  # too bad '_mt bsfm 2' does not work for all cases

  local tapeindex=$(get_tapefile_index)
  case $tapeindex in
    [01])
       mt -f $_tape_dev rewind
    ;;
    *)
       mt -f $_tape_dev bsfm 2
    ;;
  esac
}

_get_block_size()
{
  
  # In variable block mode (setblk 0), there appears to be a maximum block size.
  # For my 1st DDS3 tape it is 16K.

  local TMPF=$(mktemp /tmp/$ourname.$FUNCNAME.XXXXXXXX)

  dd bs=300K if=$_tape_dev count=1 2>&1 >/dev/null |
  tee $TMPF|
  perl -ne '
    if(m{^(\d+) bytes})
    {
      $blocksize = $1;
      $ok++;
    }
    END {
      if ($ok == 1)
      { print "$blocksize\n";}
      else
      { $?=1;
        print STDERR "'$FUNCNAME':$blocksizeERROR/failed\n";
      }
    }
  '

  local _stat=$[ ${PIPESTATUS[0]} + ${PIPESTATUS[1]} + ${PIPESTATUS[2]} ]

  [[ $_stat = 0 ]] ||
  { echo $FUNCNAME:ERROR:dd output:
    sed -e 's~^~  ~' $TMPF
    return $_stat
  }

}

_rewind()
{
  local ret_val
  (set -x;mt -f $_tape_dev rewind) ; ret_val=$?
  echo
  return $ret_val
}

_reset_tape_drive()
{
  mt -f /dev/nst0 defcompression 0
  mt -f /dev/nst0 defblksize 0
    # sets variable block size ?? 
    # explain difference w/ 'mt -f /dev/nst0 setblk 0'

  mt -f /dev/nst0 stoptions buffer-writes async-writes read-ahead
}

_GET_BLK_SIZE()
{
  BLKSIZE=$(_get_block_size)
    # global needed for dd

  [[ $BLKSIZE = 0 ]] && { echo $FUNCNAME:ERROR BLKSIZE: $BLKSIZE; return 1; } || :

  get_tape_pos
    # defines $filenum

  # goal: return tape to where it was before this function was called:
  if [[ $filenum = 0 ]] ;then
    mt -f /dev/nst0 rewind
  else
    mt -f /dev/nst0 bsfm 1
  fi
}

get_tape_file_count ()
{
  # ------------------------------------------------------------------------
  # count tape files
  # ------------------------------------------------------------------------

  # advance to end of data
  mt -f /dev/nst0 eod
  get_tape_pos
  filenum_4endof_tape=$filenum
    # $filenum_4endof_tape refers to start of nonexistent file, so turns out that
    # $filenum_4endof_tape == total number of valid files
  file_count=$filenum_4endof_tape

  if test "$[$file_count + 0]" -lt 1
  then
    echo $ourname: no tape files found
    mt -f $_tape_dev stat
    exit 1
  else
    echo "$ourname: found [$file_count] tape files"
    echo
  fi

  echo -n "rewinding .."
  mt -f /dev/nst0 rewind
  echo done
}

# **************************************************
# Main procedure.
# **************************************************
#main
{

: ${_tape_dev:=/dev/nst0}

echo -n "rewinding .."
mt -f /dev/nst0 rewind
echo done

set -x
#file_count=2
get_tape_file_count
fifo=/tmp/$ourname.$jobid
mkfifo $fifo

## read each tape file
filesreadok=0
TMPFgzip=$(mktemp /tmp/$ourname.gzip.XXXXXXXX)
TMPFdd=$(mktemp /tmp/$ourname.dd.XXXXXXXX)

trap 'rm -f $fifo $TMPFgzip $TMPFdd ' EXIT
for (( i=1; $i <= $file_count ;i += 1))
do 
  
  _GET_BLK_SIZE
  echo status prior to test:
  mt -f $_tape_dev status|sed -e 's~^~  ~'
  echo
  echo "$ourname: reading tape file [$i/$file_count] [$(date)]"
  (
    echo reading tape w/gzip -d, w/output to md5sum
    echo also counting uncompressed bytes and xfr rate w/dd read, using bs=$BLKSIZE
    echo
    time gzip -d < $_tape_dev 2>$TMPFgzip | tee $fifo |md5sum &
    dd bs=$BLKSIZE  < $fifo >/dev/null 2>$TMPFdd &
      # dd gives us total size and xfr rate
    wait
    stat=$?
    echo
    exit $stat
  ) || { stat=$?; set -x; exit $stat; }

  if [[ -s $$TMPFgzip ]];then
    echo gzip stderr:
    sed -e 's~^~  ~' $TMPFgzip
    echo
  fi

  echo dd read of uncompressed data:
  sed -e 's~^~  ~' $TMPFdd

  # TBD!!
  [[ $i = 3 ]] && exit
  let filesreadok++ || :
  echo
done

if test "$file_count" = "$filesreadok"
then
  mt -f $_tape_dev rewind
  echo "$ourname: done ok [$filesreadok/$file_count] tape files [$(date)]"
  exit 0
else
  echo "files read ok: [$filesreadok/$file_count]"
  echo "$ourname: done w/ERROR(s) [$(date)]"
  exit 1
fi

# ==================================================
# main done.
# ==================================================
: dropping through to main end #set -x makes this echo

} #main end
