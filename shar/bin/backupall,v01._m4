#!/usr/bin/env bash
set -e
set -u

# --------------------------------------------------------------------
# Synopsis: tar backup of local filesystems, rewind and read each
#   tape file. A log is created.  Remote tape drive supported.
# -----------------------------------------------------------------------------
# Usage: [_use_ssh=/usr/bin/ssh TAPE_HOST=fillin] $ourname [-(r|T)] [-d EXTRADIR1 [EXTRADIR2]..]
# -----------------------------------------------------------------------------
# Options:
#   -T         
#              test run- creates a few small files in a temp dir and 
#              backs that temp dir up several times
#   -r         
#              test run, backing "/" file system only
#   -d EXTRADIR1 [EXTRADIR2]..
#   -E                 
#              no tape eject at end
#   -D DIR1 [DIR2]..
#              ( explicitly defines dir list [no attempt to backup other 'local filesystems'])
# --------------------------------------------------------------------
# Env: see Files
# --------------------------------------------------------------------
# Files:
#    /etc/local/tape_device
#      There you may set env vars:
#        TAPE_DEVICE (default: /dev/nst0)
#        TAPE_HOST   (default: localhost)
# 
# sources:
#   $_29lib/gnutar_ok.shinc
#   $_29lib/_grep_integer_field.shinc
#   $_29lib/_tape_utils.shinc
# 
# --------------------------------------------------------------------
# Rating: tone: core tool used: often stable: y TBDs: n noteworthy: y
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2009, 2010 Tom Rodman <Rodman.T.S@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2011/12/10 20:08:11 $   (GMT)
# $Revision: 1.14 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/backup-2011.05.26/shar/bin/RCS/backupall,v $
#
# --------------------------------------------------------------------

# *****************************************************************
# define env var dir-defs for our "$_29r tree" tools
# *****************************************************************
cd ${0%/*}/../../
  # cd 2 dirs up from ( ${0%/*} == `dirname $0` )
  # if $0 has no "/"; 'source' below fails/we abort, that's OK (we do not support . in path)
_29r=$PWD
cd "$OLDPWD"

cd ${0%/*}
   # dirname of $0
_29rev=${PWD##*/}
cd "$OLDPWD"

source $_29r/package/$_29rev/main/etc/dirdefs
  # not security risk if $0 is in correct commands dir
  # dirdefs appends $_29rev suffix to some defs

source $_29lib/bash_common.shinc
  # for summary of what is provided run this:
  #   awk '/Overview-start/,/Overview-end/' $_lib/bash_common.shinc|less

# **************************************************
# Main procedure.
# **************************************************
#main
{

_email_on_unexpected_exit=${_email_on_unexpected_exit:-yes}
_email_if_dying_on_trapped_sig=${_email_if_dying_on_trapped_sig:-no}
# _email_if_errlog_exists=yes # for Example "errlog foo" => e-mail

# =========================================================
# log creation and purging
# =========================================================
log=$(mk_log_expire_named -e 200)
  # logname contains a time to live string; -e 30 => expire after 30 days

# --------------------------------------------------------------------{
# ( optional block for configuring script w/rcsfile
#
#   read rcfile ( for env var defs );
#   local rcfile for local setting, else look for site setting
#
#   what dirs $_29eloc and $_29team are for:
#     awk '/## shared script rc files/, /^#-/' $_m/etc/dirdefs
#
# --------------------------------------------------------------------
for dir in $_29eloc $_29team
do
  rcfile=$dir/${ourname}rc
    # env vars impacted by rcfile:
    #   _use_ssh (if defined is path to ssh, typically /usr/bin/ssh )

  if test -s "$rcfile"
  then
    source "$rcfile"
    break #prefer local file
  fi
done
unset rcfile dir
# }

# **************************************************
# script start:
# **************************************************

test -s /etc/local/tape_device && source /etc/local/tape_device
tape_dev=${TAPE_DEVICE:-/dev/nst0}
tape_host=${TAPE_HOST:-}

opt_true=1 OPTIND=1 OPT_E= OPT_T= OPT_r= OPT_d= OPT_D=
  # OPTIND=1 only needed for 2nd and subsequent getopt invocations
  # since shell's start up sets it to 1
while getopts :ETrd:D: opt_char
  do
     # Check for errors.
     case $opt_char in
       \?) >&2 echo "Unexpected option(-$OPTARG)."
           exit 1;
           ;;
        T)testrun=1 ;;
        r)testrun=1 
          rootdironly=1
            # it creates some tar STDERR, but is small enough for a test
        ;;
     esac

     # Record the info in an "OPT_*" env var.
     eval OPT_${opt_char}="\"\${OPTARG:-$opt_true}\""
  done

shift $(( $OPTIND -1 ))
unset opt_true opt_char

sup_dirs=${OPT_d:-}
noeject=${OPT_E:-}

source $_29lib/gnutar_ok.shinc

source $_29lib/_grep_integer_field.shinc

source $_29lib/_tape_utils.shinc

## main
{

echo tape_host: [$tape_host]

echo trying rewind ..
delay=(6 16 32 64 128 256)
cnt=0
until _mt rewind
do 
  if test "$cnt" = 6
  then 
    echo giving up on rewind..
    exit
  fi

  if test $cnt -lt 3
  then
    echo pls insert tape
  fi

  (set -x; sleep ${delay[$cnt]})
  let cnt++ || :
  echo `date` trying to rewind again..
done

echo tape rewound OK ..
sleep 2
echo

if test "${testrun:-}" = 1
then
  # testrun backup
  if test "${rootdironly:-}" = 1
  then
    # backup "/"
    directories=/
  else
    # tiny backup
    testdir=/tmp/$ourname
    mkdir -p $testdir
    seq 100000 >  $testdir/foo$$
    directories="$testdir $testdir $testdir"
  fi
elif [[ -n ${OPT_D:-} ]];then
  directories=$OPT_D
else
  directories=$($_29c/mount-points-local)

  # remove /tmp, ie do not backup /tmp partition
  new_directories=""
  for d in $directories;do
    if [[ $d == /tmp ]];then :
    else
      new_directories+=" $d"
    fi
  done 
  directories="$new_directories"

  directories="${sup_dirs:-} $directories"
fi

###
## start the backup ##
###

if [[ -n ${tape_host:-} ]];then
  echo env var tape_host is defined as: [$tape_host]
  echo Defining tape_host suggests the tape drive is on that remote host.
  echo
fi

MSG=$(
  echo "### $(date) cur host: $(_hostname_short), summary/goal is to backup:"
  for part in $directories;do
    echo "  [$part]"
  done
  echo on $(_hostname_short)
  echo
)

echo "$MSG"

# save "$status_file" to tap
TMPDIR=$(mktemp -d /tmp/$ourname.XXXXX)
summary="host#$(_hostname_short):dirsbacked#"
for part in $directories;do
  part=${part//\//,,}
  summary+=${part}+
done
summary=${summary%+}
status_file="$summary"

(
set -e
cd $TMPDIR
echo "$MSG" >  "$status_file"

set -x
: Save "$status_file" to tape:
tar ${_use_ssh+--rsh-command=$_use_ssh}    -zcf ${tape_host:+${tape_host}:}${tape_dev} "$status_file"
) || exit $?

rm -f "$TMPDIR/$status_file"
rmdir "$TMPDIR"

## main run
tar_run=0
for part in $directories
do

  echo mt stat just prior to tar run-number: $tar_run
  _mt stat

  tar_filesystem $part

  # check our backup
  rewind_1_file
  untar_1tapefile_to_devnull
  let tar_run++ || :

done

echo
#_mt eject
test -n "${testrun:-${noeject:-}}" || _mt offline
echo all done at `date`

} 2>&1 |tee $log

exit 0

###### ##### ##### ^^^^^ ###   END OF SCRIPT   ### ^^^^^ ##### ##### ##### ##### 
# ==================================================
# main done.
# ==================================================
: dropping through to main end #set -x makes this echo
exit 0

} #main end

