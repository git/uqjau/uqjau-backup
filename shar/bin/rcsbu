#!/usr/bin/env bash
set -eu

# --------------------------------------------------------------------
# Synopsis: Recursively find rcs source and history files in specified
# dirs, tar RCS ",v" files (history archives), and related current
# standalone (non RCS) version; archive these files to a "rotation-named"
# tar.gz file.
# --------------------------------------------------------------------
# Usage and Env
#   see help
# --------------------------------------------------------------------
# Rating: tone: tool used: often provincial: somewhat stable: y notable: y
# --------------------------------------------------------------------

help()
{
cat <<__HELP_EOF

-${ourname}-
Synopsis: tar RCS ,v files (RCS versions archives), and related current version.  

Usage: 
  $ourname [-f TAR-GZIP-ARCHIVE] dir1 [dir2]..
  $ourname -l[D] dir1 [dir2]..

Note: Should stay single filesystem for each: dir1 [dir2]..

OPTIONS

  -l    list RCS working and archive files, and (by default)
        a separate line for each dir

  -D    do not include directories themselves in the list
        only makes sense to use w/ "-l".  The default is
        to pass 1 line for each dir to "tar -T" so that
        tar saves dir permissions also.

  -v    verbose

Ex
  $ourname -f /tmp/root_and_usr-rcsbu.tar.gz / /usr

Ex list files w/rcs archives
  $ourname -lD /etc

FILE
  $_29eloc/${ourname}rc
    # - to define \$recovery for cron job
    # - may redefine array: dirs2back

ENV
   recovery used for default value of \$archive, as in:
     archive=\${recovery}/\$hostname/$ourname
   bash array: dirs2back  (search ahead for default)

__HELP_EOF

exit 0
}

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2010/10/09 12:53:09 $   (GMT)
# $Revision: 1.20 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/backup/shar/bin/RCS/rcsbu,v $
#      $Log: rcsbu,v $
#      Revision 1.20  2010/10/09 12:53:09  rodmant
#      *** empty log message ***
#
#      Revision 1.19  2010/05/01 13:19:41  rodmant
#      *** empty log message ***
#
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2009,2010 Tom Rodman <Rodman.T.S@gmail.com>
# -------------------------------------------------------------------- 

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

# *****************************************************************
# define env var dir-defs for our "uqjau tools"
# *****************************************************************

if [[ -n ${BASH_SOURCE[0]} ]];then
    # We are being sourced.
    cd ${BASH_SOURCE[0]%/*}/../../
else
    cd ${0%/*}/../../
        # cd 2 dirs up from ( ${0%/*} == $(dirname $0)
        # if $0 has no "/"; 'source' below fails/we abort, that's OK (we do not support . in path)
fi

_29r=$PWD
cd "$OLDPWD"

if [[ -n ${BASH_SOURCE[0]} ]];then
    cd ${BASH_SOURCE[0]%/*}
else
    cd ${0%/*}
        # $(dirname $0)
fi

_29rev=${PWD##*/}
   # Basename of dir below 'commands', 'scommands' or 'lib'; it contains the soft link
   # to this script.
cd "$OLDPWD"

if ! test -f $_29r/package/$_29rev/main/etc/dirdefs; then
  # create default dirdefs 
  source $_29r/package/$_29rev/main/slib/seed_dirdefs.shinc
else
  source $_29r/package/$_29rev/main/etc/dirdefs
    # not security risk if $0 is in correct commands dir
    # dirdefs appends $_29rev suffix to some defs
fi

source $_29lib/bash_common.shinc
  # for summary of what is provided run this:
  #   awk '/Overview-start/,/Overview-end/' $_lib/bash_common.shinc

case ${1:-} in
  --[hH][eE][lL][pP]) help "$@" ;exit ;;
esac

archive=""
OPTIND=1 opt_true=1
unset OPT_l OPT_f OPT_D VERBOSE
while getopts :vlf:D opt_char
do
   case $opt_char in
     l)
       list_only=yes
     ;;
     f)
       archive=$OPTARG
       touch $archive || exit 1
     ;;
     v)
       VERBOSE=1
     ;;
   esac
   # Record the info in an "OPT_*" env var.
   eval OPT_${opt_char}="\"\${OPTARG:-$opt_true}\""
done
shift $(( $OPTIND -1 ))
unset OPTARG opt_char opt_true

# **************************************************
# external scripts used
# **************************************************

  FIND_RCS_FILES=$_29c/_find_rcs-files
  pathstems_uniq_filter=$_29c/pathstems_uniq.filter
  period_in_revolution=$_29c/period_in_revolution

  source $_29lib/gnutar_ok.shinc

# **************************************************
# Main procedure.
# **************************************************
#main
{

_email_on_unexpected_exit=${_email_on_unexpected_exit:-yes}
_email_if_dying_on_trapped_sig=${_email_if_dying_on_trapped_sig:-no}

_email_on_unexpected_exit=${_email_on_unexpected_exit:-yes}
_email_if_errlog_exists=yes

dirs2back=("$@")

# --------------------------------------------------------------------
# read rcfile if it exists
# local for local setting, else look for site setting
# --------------------------------------------------------------------
for dir in $_29eloc $_29team
do
  rcfile=$dir/${ourname}rc
  if test -f "$rcfile";then
    source "$rcfile" 
      # - to define $recovery for cron job
      # - may redefine array dirs2back
    break 
      #prefer local file
  fi
done
unset rcfile dir

hostname=$(_hostname_short)

if ! test "${list_only:-}" = "yes"
then
#{
  if test ! -n "$archive" 
  then
    archive=${recovery}/$hostname/$ourname
    mkdir -p $archive
    archive_slink=$archive/$ourname.tz
    archive=$archive/$ourname.tz.$($period_in_revolution -d 7 -p 12 )
      # suffix varies from 0-11 every 7 * 12 = 94 days
  else
    archive_slink=""
  fi

  archive_temp=${archive}.$jobid  
    # to preserve any prexisting $archive file until new archive fully made
#}
fi

_mktmpfiles 3
files2bu=$tmpf0

# --------------------------------------------------------------------
# dirs2back is an array of the directories search
# --------------------------------------------------------------------

if test ${#dirs2back[*]} -lt 1
then
  #dirs2back=(/etc /usr/local /var)
  dirs2back=(/ /usr /var)
fi

test -n "${VERBOSE:-}" && \
(
echo dirs2back: ${dirs2back[@]}  
sleep 2
) >&2

# --------------------------------------------------------------------
# create list of files to backup
# --------------------------------------------------------------------
test -n "${VERBOSE:-}" && \
(
test "${list_only:-}" = yes || echo planning to save files in [$archive]
echo creating list of objects ..
) >&2

$FIND_RCS_FILES  "${dirs2back[@]}" >  $tmpf1
test -n "${VERBOSE:-}" && echo

# --------------------------------------------------------------------
# add in all parent directories to list so
# dir perms are saved
# --------------------------------------------------------------------
($pathstems_uniq_filter <$tmpf1 ;cat $tmpf1 ) | sort -u > $files2bu

if test ! -s "$files2bu"
then
   errlog no files to backup found  >&2
   exit 1
fi

if test "${list_only:-}" = "yes"
then
  if test "${OPT_D:-}"
  then
    egrep -v /RCS $tmpf1
      # show only source files or symbolic links; no archives
  else
    cat $files2bu
  fi
  exit 0
fi

cnt=$(echo $(wc -l <$tmpf1))

test -n "${VERBOSE:-}" && \
(
sleep_delay=5s
echo $ourname:$(date): in $sleep_delay, creating tar gzipped archive w/$cnt items:
echo "  [$archive]"
sleep $sleep_delay
echo
) >&2

tar_err_log=$tmpf2

set +e
(set -x; tar --no-recursion -zcf ${archive_temp} -T "$files2bu" 2>$tar_err_log)
tar_stat=$?
set -e

echo "tar_stat: [$tar_stat]"

mv -f ${archive_temp} "$archive"

if gnutar_ok < "$tar_err_log"
then
  final_stat=0
else
  final_stat=1
  (set -x;cat $tar_err_log)
fi

test -n "${VERBOSE:-}" && \
(
echo "$ourname: tar returned: [$tar_stat]"
ls -lh "$archive" 
echo
) >&2

if test -s "$tar_err_log"
then
  if test "$final_stat" = 0
  then
    err_type="acceptable"
  else
    err_type="un-acceptable"
    _save_tempfiles=1
  fi

  {
  echo "($err_type) tar complete-unfiltered-STDERR output:"
  sed -e 's~^~  ~' $tar_err_log
  } >&2
fi

test -n "${VERBOSE:-}" && echo >&2

# add convenience soft link, if "$archive" was given rotation style name
  if test -n "${archive_slink:-}"
  then
    test -f $archive_slink && chown $(id -un) $archive_slink #for cygwin
    rm -f "$archive_slink"
    ln -s "$archive" "$archive_slink"
    ls -ld "$archive" "$archive_slink"
  fi

exit $final_stat

# ==================================================
# main done.
# ==================================================
: dropping through to main end #set -x makes this echo
exit 0

} #main end
